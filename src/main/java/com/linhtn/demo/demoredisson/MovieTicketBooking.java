package com.linhtn.demo.demoredisson;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.MDC;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @created 18/05/2024 - 2:33 PM
 * @project: demo-redisson
 * @author: linhtn
 */
@Slf4j
public class MovieTicketBooking {
    private static final String LOCK_KEY = "ticketLock";
    private static final String TICKET_KEY = "movieTicket:availableTickets";


    public static RedissonClient redissonClient() throws IOException {
        Config config = Config.fromYAML(MovieTicketBooking.class.getClassLoader().getResource("redisson.yaml"));
        return Redisson.create(config);
    }

    public static void main(String[] args) throws IOException {
        RedissonClient redisson = redissonClient();
        setUpTickets(redisson);
        // Giả lập nhiều người dùng mua vé
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                buyTicket(redisson);
            }).start();
        }
    }

    public static void setUpTickets(RedissonClient redisson) {
        RBucket<Integer> bucket = redisson.getBucket(TICKET_KEY);
        bucket.get();
        if (bucket.get() <= 0) {
            // nếu hết số lượng vé thì reset
            bucket.set(100);
        }
    }

    public static void buyTicket(RedissonClient redisson) {
        //RLock lock = redisson.getLock(LOCK_KEY);
        RLock lock = redisson.getFairLock(LOCK_KEY);
        try {
            // Thử lấy khóa với timeout
            //lock.lock(10, TimeUnit.SECONDS);
            // đợi trong vòng 1s (waitTime) để lấy được khóa, nếu có khóa giữ nó trong 10s (leaseTime), sau đó tự động unlock
            // nếu k có khóa nào trong vòng 1s (waitTime), trả về false
            log.info("Init {}", Thread.currentThread().getName());
            boolean isLocked = lock.tryLock(1, 10, TimeUnit.SECONDS);
            if (isLocked) {
                try {
                    // Giả lập độ trễ N giây
                    Thread.sleep(1000);

                    // Kiểm tra và cập nhật số lượng vé
                    int availableTickets = Integer.parseInt(redisson.getBucket(TICKET_KEY).get().toString());
                    if (availableTickets > 0) {
                        redisson.getBucket(TICKET_KEY).set(availableTickets - 1);
                        log.info(Thread.currentThread().getName() + " mua vé thành công. Số vé còn lại: " + (availableTickets - 1));
                    } else {
                        log.info(Thread.currentThread().getName() + " không mua được vé, hết vé.");
                    }
                } finally {
                    lock.unlock();
                    log.info("finish buy ticket and lock.unlock();");
                }
            } else {
                log.info(Thread.currentThread().getName() + " không thể lấy khóa.");
            }
        } catch (InterruptedException e) {
            log.error("Error: ", e);
        }
    }

    public static void buyTicketWithoutLock(RedissonClient redisson) {
        MDC.put("traceId", String.valueOf(Thread.currentThread().threadId()));
        log.info("Init {}", Thread.currentThread().getName());

        // Kiểm tra và cập nhật số lượng vé
        int availableTickets = Integer.parseInt(redisson.getBucket(TICKET_KEY).get().toString());
        if (availableTickets > 0) {
            // giả lập book vé
            log.info("booking...");
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            redisson.getBucket(TICKET_KEY).set(availableTickets - 1);
            log.info(Thread.currentThread().getName() + " mua vé thành công. Số vé còn lại: " + (availableTickets - 1));
        } else {
            log.info(Thread.currentThread().getName() + " không mua được vé, hết vé.");
        }

    }
}
